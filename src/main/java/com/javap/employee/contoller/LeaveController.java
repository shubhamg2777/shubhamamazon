package com.javap.employee.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.javap.employee.constants.LeaveConstants;
import com.javap.employee.dto.ResponseDto;
import com.javap.employee.entity.LeaveEntity;
import com.javap.employee.service.LeaveService;

@RestController
@RequestMapping(value = LeaveConstants.LEAVE_CONTROLLER)
public class LeaveController {
	
	@Autowired
	private LeaveService service;
	
	@Autowired
	RestTemplate restTemplate;
	
	@PostMapping(value = LeaveConstants.SAVE_LEAVE)
	public @ResponseBody ResponseDto save(@RequestBody LeaveEntity leaveEntity)
	{
		System.out.println("i am a fullStack java developer");
		
		
		return  service.save(leaveEntity);
		
	}

}
