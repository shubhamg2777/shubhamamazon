package com.javap.employee.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name ="leave_details")
public class LeaveEntity implements Serializable {
	
	@Id
	@GenericGenerator(name = "auto",strategy = "increment")
	@GeneratedValue(generator = "auto")
	
    @Column(name="id")
	private long id;
	
	
	@Column(name="employeeid")
	private long employee_Id;
	
	@Column(name="employeeStartDate")
	private Date employee_Start_Date;
	
	@Column(name="employeeEndDate")
    private Date employee_End_Date;
	
	@Column(name="totalDays")
	private long total_Days;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getEmployee_Id() {
		return employee_Id;
	}

	public void setEmployee_Id(long employee_Id) {
		this.employee_Id = employee_Id;
	}

	public Date getEmployee_Start_Date() {
		return employee_Start_Date;
	}

	public void setEmployee_Start_Date(Date employee_Start_Date) {
		this.employee_Start_Date = employee_Start_Date;
	}

	public Date getEmployee_End_Date() {
		return employee_End_Date;
	}

	public void setEmployee_End_Date(Date employee_End_Date) {
		this.employee_End_Date = employee_End_Date;
	}

	public long getTotal_Days() {
		return total_Days;
	}

	public void setTotal_Days(long total_Days) {
		this.total_Days = total_Days;
	}
	
	@Override
	public String toString() {
		return "LeaveEntity [id=" + id + ", employee_Id=" + employee_Id + ", employee_Start_Date=" + employee_Start_Date
				+ ", employee_End_Date=" + employee_End_Date + ", total_Days=" + total_Days + "]";
	}

	
	

}
